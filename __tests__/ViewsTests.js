
import React from 'react'
import { shallow } from 'enzyme'
import footer, { wordOfTheDayFooter } from './../src/views/Footers'
import anagramsList from './../src/views/AnagramsList'
import audioFooter from './../src/views/AudioFooter'
// import renderBannerAd from './../src/views/BannerAd'
import definitionsList from './../src/views/DefinitionsList'
import examplesList from './../src/views/ExamplesList'
import plainHeader, {
  rhymeHeader,
  similarHeader,
  synonymHeader,
  antonymHeader,
  exampleHeader,
  historyHeader,
  definitionHeader,
  anagramHeader } from './../src/views/Headers'
import historyList from './../src/views/HistoryList'
import listEmptyView from './../src/views/ListEmptyView'
import loading from './../src/views/Loading'
import rhymeList from './../src/views/RhymeList'
import similarListRD from './../src/views/SimilarListRD'
import wotdView from './../src/views/WOTDView'

jest.mock('react-native-firebase', () => {
  return {
    initializeApp: jest.fn()
  }
})

// TypeError: Cannot read property 'admob' of undefined
describe('renders correctly', () => {
  it('should render footer as expected', () => {
    const wrapper = shallow(<footer />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render wordOfTheDayFooter as expected', () => {
    const wrapper = shallow(<wordOfTheDayFooter />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render anagramsList as expected', () => {
    const wrapper = shallow(<anagramsList />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render audioFooter as expected', () => {
    const wrapper = shallow(<audioFooter />)
    expect(wrapper).toMatchSnapshot()
  })

  xit('should render renderBannerAd as expected', () => {
    const wrapper = shallow(<renderBannerAd />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render definitionsList as expected', () => {
    const wrapper = shallow(<definitionsList />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render examplesList as expected', () => {
    const wrapper = shallow(<examplesList />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render plainHeader as expected', () => {
    const wrapper = shallow(<plainHeader />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render rhymeHeader as expected', () => {
    const wrapper = shallow(<rhymeHeader />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render similarHeader as expected', () => {
    const wrapper = shallow(<similarHeader />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render synonymHeader as expected', () => {
    const wrapper = shallow(<synonymHeader />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render antonymHeader as expected', () => {
    const wrapper = shallow(<antonymHeader />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render exampleHeader as expected', () => {
    const wrapper = shallow(<exampleHeader />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render historyHeader as expected', () => {
    const wrapper = shallow(<historyHeader />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render definitionHeader as expected', () => {
    const wrapper = shallow(<definitionHeader />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render anagramHeader as expected', () => {
    const wrapper = shallow(<anagramHeader />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render historyList as expected', () => {
    const wrapper = shallow(<historyList />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render listEmptyView as expected', () => {
    const wrapper = shallow(<listEmptyView />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render loading as expected', () => {
    const wrapper = shallow(<loading />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render rhymeList as expected', () => {
    const wrapper = shallow(<rhymeList />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render similarListRD as expected', () => {
    const wrapper = shallow(<similarListRD />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render wotdView as expected', () => {
    const wrapper = shallow(<wotdView />)
    expect(wrapper).toMatchSnapshot()
  })
})

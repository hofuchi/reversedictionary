
import React from 'react'
import { shallow } from 'enzyme'
import { clearHistory, clearHistorySilent } from './../src/settings/ClearHistory'
import { loadAllSettings, loadHistory } from './../src/settings/Settings'
import {
  anagramsSwitched,
  historySwitched,
  sourceSwitched,
  rhymesSwitched,
  antonymsSwitched,
  synonymsSwitched,
  similarSwitched,
  pagingSwitched,
  examplesSwitched,
  hideEmptyPages,
  adsSwitched } from './../src/settings/Switches'

describe('renders correctly', () => {
  it('should render clearHistory as expected', () => {
    const wrapper = shallow(<clearHistory />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render clearHistorySilent as expected', () => {
    const wrapper = shallow(<clearHistorySilent />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render loadAllSettings as expected', () => {
    const wrapper = shallow(<loadAllSettings />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render loadHistory as expected', () => {
    const wrapper = shallow(<loadHistory />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render anagramsSwitched as expected', () => {
    const wrapper = shallow(<anagramsSwitched />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render historySwitched as expected', () => {
    const wrapper = shallow(<historySwitched />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render sourceSwitched as expected', () => {
    const wrapper = shallow(<sourceSwitched />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render rhymesSwitched as expected', () => {
    const wrapper = shallow(<rhymesSwitched />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render antonymsSwitched as expected', () => {
    const wrapper = shallow(<antonymsSwitched />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render synonymsSwitched as expected', () => {
    const wrapper = shallow(<synonymsSwitched />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render similarSwitched as expected', () => {
    const wrapper = shallow(<similarSwitched />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render pagingSwitched as expected', () => {
    const wrapper = shallow(<pagingSwitched />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render examplesSwitched as expected', () => {
    const wrapper = shallow(<examplesSwitched />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render hideEmptyPages as expected', () => {
    const wrapper = shallow(<hideEmptyPages />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render adsSwitched as expected', () => {
    const wrapper = shallow(<adsSwitched />)
    expect(wrapper).toMatchSnapshot()
  })
})

const settings = {
  sourceIsWordnik: false,
  antonymsVisible: true,
  rhymesVisible: true,
  synonymsVisible: true,
  similarVisible: true,
  pagingEnabled: true,
  examplesVisible: true,
  historyDisabled: false,
  hideEmptyPages: true,
  ads: false,
  usageStats: true,
  anagramsVisible: true
}

describe('returns correct value', () => {
  xit('should return the correct value for loadAllSettings', () => {
    expect(loadAllSettings()).toBe(true)
  })

  xit('should return the correct value for loadHistory', () => {
    expect(loadHistory()).toBe(true)
  })

  // I need to copy the settings object, (not reference)
  // ... so early tests don't change the
  // ... object for later tests
  it('should return the correct value for anagramsSwitched', () => {
    expect(anagramsSwitched(settings)).toMatchObject({
      sourceIsWordnik: false,
      antonymsVisible: true,
      rhymesVisible: true,
      synonymsVisible: true,
      similarVisible: true,
      pagingEnabled: true,
      examplesVisible: true,
      historyDisabled: false,
      hideEmptyPages: true,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for historySwitched', () => {
    expect(historySwitched(settings)).toMatchObject({
      sourceIsWordnik: false,
      antonymsVisible: true,
      rhymesVisible: true,
      synonymsVisible: true,
      similarVisible: true,
      pagingEnabled: true,
      examplesVisible: true,
      historyDisabled: true,
      hideEmptyPages: true,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for sourceSwitched', () => {
    expect(sourceSwitched(settings)).toMatchObject({
      sourceIsWordnik: true,
      antonymsVisible: true,
      rhymesVisible: true,
      synonymsVisible: true,
      similarVisible: true,
      pagingEnabled: true,
      examplesVisible: true,
      historyDisabled: true,
      hideEmptyPages: true,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for rhymesSwitched', () => {
    expect(rhymesSwitched(settings)).toMatchObject({
      sourceIsWordnik: true,
      antonymsVisible: true,
      rhymesVisible: false,
      synonymsVisible: true,
      similarVisible: true,
      pagingEnabled: true,
      examplesVisible: true,
      historyDisabled: true,
      hideEmptyPages: true,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for antonymsSwitched', () => {
    expect(antonymsSwitched(settings)).toMatchObject({
      sourceIsWordnik: true,
      antonymsVisible: false,
      rhymesVisible: false,
      synonymsVisible: true,
      similarVisible: true,
      pagingEnabled: true,
      examplesVisible: true,
      historyDisabled: true,
      hideEmptyPages: true,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for synonymsSwitched', () => {
    expect(synonymsSwitched(settings)).toMatchObject({
      sourceIsWordnik: true,
      antonymsVisible: false,
      rhymesVisible: false,
      synonymsVisible: false,
      similarVisible: true,
      pagingEnabled: true,
      examplesVisible: true,
      historyDisabled: true,
      hideEmptyPages: true,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for similarSwitched', () => {
    expect(similarSwitched(settings)).toMatchObject({
      sourceIsWordnik: true,
      antonymsVisible: false,
      rhymesVisible: false,
      synonymsVisible: false,
      similarVisible: false,
      pagingEnabled: true,
      examplesVisible: true,
      historyDisabled: true,
      hideEmptyPages: true,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for pagingSwitched', () => {
    expect(pagingSwitched(settings)).toMatchObject({
      sourceIsWordnik: true,
      antonymsVisible: false,
      rhymesVisible: false,
      synonymsVisible: false,
      similarVisible: false,
      pagingEnabled: false,
      examplesVisible: true,
      historyDisabled: true,
      hideEmptyPages: true,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for examplesSwitched', () => {
    expect(examplesSwitched(settings)).toMatchObject({
      sourceIsWordnik: true,
      antonymsVisible: false,
      rhymesVisible: false,
      synonymsVisible: false,
      similarVisible: false,
      pagingEnabled: false,
      examplesVisible: false,
      historyDisabled: true,
      hideEmptyPages: true,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for hideEmptyPages', () => {
    expect(hideEmptyPages(settings)).toMatchObject({
      sourceIsWordnik: true,
      antonymsVisible: false,
      rhymesVisible: false,
      synonymsVisible: false,
      similarVisible: false,
      pagingEnabled: false,
      examplesVisible: false,
      historyDisabled: true,
      hideEmptyPages: false,
      ads: false,
      usageStats: true,
      anagramsVisible: false
    })
  })

  it('should return the correct value for adsSwitched', () => {
    expect(adsSwitched(settings)).toMatchObject({
      sourceIsWordnik: true,
      antonymsVisible: false,
      rhymesVisible: false,
      synonymsVisible: false,
      similarVisible: false,
      pagingEnabled: false,
      examplesVisible: false,
      historyDisabled: true,
      hideEmptyPages: false,
      ads: true,
      usageStats: false,
      anagramsVisible: false
    })
  })
})

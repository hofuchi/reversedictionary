import React from 'react'
import { shallow } from 'enzyme'
import SearchResult from './../src/components/SearchResultPage'

describe('Testing SearchResult component', () => {
  it('renders as expected', () => {
    const wrapper = shallow(
      <SearchResult
        word='foobar'
        settings={{
          sourceIsWordnik: false,
          antonymsVisible: true,
          rhymesVisible: true,
          synonymsVisible: true,
          similarVisible: true,
          pagingEnabled: true,
          examplesVisible: true,
          historyDisabled: false,
          hideEmptyPages: true,
          ads: false,
          usageStats: true,
          anagramsVisible: true
        }}
      />
    )
    expect(wrapper).toMatchSnapshot()
  })
})


import React from 'react'
import { shallow } from 'enzyme'
import aboutPageStyles from './../src/styles/AboutPageStyles'
import mainStyles from './../src/styles/MainStyles'
import searchPageStyles from './../src/styles/SearchPageStyles'
import searchResultPageStyles from './../src/styles/SearchResultPageStyles'
import settingsPageStyles from './../src/styles/SettingsPageStyles'
import {
  searchOverrides,
  searchResultOverrides } from './../src/styles/Overrides'

describe('renders correctly', () => {
  it('should render aboutPageStyles as expected', () => {
    const wrapper = shallow(<aboutPageStyles />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render mainStyles as expected', () => {
    const wrapper = shallow(<mainStyles />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render searchPageStyles as expected', () => {
    const wrapper = shallow(<searchPageStyles />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render searchResultPageStyles as expected', () => {
    const wrapper = shallow(<searchResultPageStyles />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render settingsPageStyles as expected', () => {
    const wrapper = shallow(<settingsPageStyles />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render searchOverrides as expected', () => {
    const wrapper = shallow(<searchOverrides />)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render searchResultOverrides as expected', () => {
    const wrapper = shallow(<searchResultOverrides />)
    expect(wrapper).toMatchSnapshot()
  })
})

/*
describe('returns correct value', () => {
  it('should return the correct bool (true) for backAndroid', () => {
      expect(backAndroid()).toBe(true);
  });
})
*/

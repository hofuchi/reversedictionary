
import React from 'react'
// import sinon from 'sinon';
import { shallow } from 'enzyme'
// import Search from './../src/components/SearchPage'

jest.mock('react-native-firebase', () => {
  return {
    initializeApp: jest.fn()
  }
})

// TypeError: Cannot read property 'admob' of undefined
describe('Testing Search component', () => {
  xit('renders as expected', () => {
    const wrapper = shallow(
      <Search />
    )
    expect(wrapper).toMatchSnapshot()

    // const onChangeTextSpy = sinon.spy();
    const render = wrapper.dive()
    render.find('TextField').forEach(child => {
      child.simulate('onChangeText')
    })
    render.find('RaisedTextButton').forEach(child => {
      child.simulate('onPress')
    })
  })
})

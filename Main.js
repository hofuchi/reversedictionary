import React, { Component } from 'react'
import Search from './src/components/SearchPage'
import About from './src/components/AboutPage'
import SearchResult from './src/components/SearchResultPage'
import Settings from './src/components/SettingsPage'
import {
  Scene,
  Router } from 'react-native-router-flux'
import {
  refreshOnBack,
  homeButton,
  aboutButton,
  settingsButton,
  reducerCreate } from './src/constants/MainConstants'
// Might switch to realm for storing settings for speed over AsyncStorage

class Main extends Component {
  render () {
    // Multiple searchResult scenes were used as a hackish way to push
    // ... the same scene while using react-native-router-flux
    return (
      <Router createReducer={reducerCreate}>
        <Scene key='root'>
          <Scene key='search' initial onEnter={refreshOnBack} component={Search} title='Reverse Dictionary' renderRightButton={aboutButton} renderLeftButton={settingsButton} />
          <Scene key='searchResult' component={SearchResult} title='Reverse Dictionary' renderRightButton={homeButton} />
          <Scene key='searchResult2' component={SearchResult} title='Reverse Dictionary' renderRightButton={homeButton} />
          <Scene key='searchResult3' component={SearchResult} title='Reverse Dictionary' renderRightButton={homeButton} />
          <Scene key='searchResult4' component={SearchResult} title='Reverse Dictionary' renderRightButton={homeButton} />
          <Scene key='searchResult5' component={SearchResult} title='Reverse Dictionary' renderRightButton={homeButton} />
          <Scene key='about' component={About} title='About' renderRightButton={homeButton} />
          <Scene key='settings' component={Settings} title='Settings' renderRightButton={homeButton} />
        </Scene>
      </Router>
    )
  }
}

export default Main

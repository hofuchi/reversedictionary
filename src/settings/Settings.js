
import { AsyncStorage } from 'react-native'

async function loadAllSettings () {
  try {
    var settings = JSON.parse(await AsyncStorage.getItem('settings'))
    if (Object.keys(settings).length < 15) {
      throw Error('Not all settings loaded, resetting settings')
    }
  } catch (err) { // Some previous settings not saved, sets settings to saved or default
    if (settings === null) { // New user
      settings = {}
    }

    settings = {
      sourceIsWordnik: settings.hasOwnProperty("sourceIsWordnik") ? settings.sourceIsWordnik : false,
      antonymsVisible: settings.hasOwnProperty("antonymsVisible") ? settings.antonymsVisible : true,
      rhymesVisible: settings.hasOwnProperty("rhymesVisible") ? settings.rhymesVisible : true,
      synonymsVisible: settings.hasOwnProperty("synonymsVisible") ? settings.synonymsVisible : true,
      similarVisible: settings.hasOwnProperty("similarVisible") ? settings.similarVisible : true,
      pagingEnabled: settings.hasOwnProperty("pagingEnabled") ? settings.pagingEnabled : true,
      examplesVisible: settings.hasOwnProperty("examplesVisible") ? settings.examplesVisible : true,
      historyDisabled: settings.hasOwnProperty("historyDisabled") ? settings.historyDisabled : false,
      hideEmptyPages: settings.hasOwnProperty("hideEmptyPages") ? settings.hideEmptyPages : true,
      ads: settings.hasOwnProperty("ads") ? settings.ads : false,
      usageStats: settings.hasOwnProperty("usageStats") ? settings.usageStats : true,
      anagramsVisible: settings.hasOwnProperty("anagramsVisible") ? settings.anagramsVisible : true,
      offlineCache: settings.hasOwnProperty("offlineCache") ? settings.offlineCache : false,
      wwfVisible: settings.hasOwnProperty("wwfVisible") ? settings.wwfVisible : false,
      historyLength: settings.hasOwnProperty("historyLength") ? settings.historyLength : 90
    }

    await AsyncStorage.setItem('settings', JSON.stringify(settings))
  }
  // Lookup if there's a better way to return asyncronous data withoug a promise
  return new Promise(resolve => {
    resolve(settings)
  })
}

async function loadHistory () {
  return JSON.parse(await AsyncStorage.getItem('history'))
}

export { loadAllSettings, loadHistory }

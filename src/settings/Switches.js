
function historySwitched (settings) {
  // The history-enabled? switch is toggled
  if (settings.historyDisabled) {
    settings.historyDisabled = false
  } else {
    settings.historyDisabled = true
  }
  return settings
}

function sourceSwitched (settings) {
  // The data source switch is toggled
  if (settings.sourceIsWordnik) {
    settings.sourceIsWordnik = false
  } else {
    settings.sourceIsWordnik = true
  }
  return settings
}

function rhymesSwitched (settings) {
  // The rhymes-enabled? switch is toggled
  if (settings.rhymesVisible) {
    settings.rhymesVisible = false
  } else {
    settings.rhymesVisible = true
  }
  return settings
}

function antonymsSwitched (settings) {
  // The antonyms-enabled? switch is toggled
  if (settings.antonymsVisible) {
    settings.antonymsVisible = false
  } else {
    settings.antonymsVisible = true
  }
  return settings
}

function synonymsSwitched (settings) {
  // The synonyms-enabled? switch is toggled
  if (settings.synonymsVisible) {
    settings.synonymsVisible = false
  } else {
    settings.synonymsVisible = true
  }
  return settings
}

function similarSwitched (settings) {
  // The similar-enabled? switch is toggled
  if (settings.similarVisible) {
    settings.similarVisible = false
  } else {
    settings.similarVisible = true
  }
  return settings
}

function pagingSwitched (settings) {
  // Paging switches from pages to continuous horizontal scroll
  if (settings.pagingEnabled) {
    settings.pagingEnabled = false
  } else {
    settings.pagingEnabled = true
  }
  return settings
}

function examplesSwitched (settings) {
  // The examples-enabled? switch is toggled
  if (settings.examplesVisible) {
    settings.examplesVisible = false
  } else {
    settings.examplesVisible = true
  }
  return settings
}

function hideEmptyPages (settings) {
  // If enabled, hides pages when the API returns no data for the request
  if (settings.hideEmptyPages) {
    settings.hideEmptyPages = false
  } else {
    settings.hideEmptyPages = true
  }
  return settings
}

function adsSwitched (settings) {
  // Enables ads, if disabled -- usage statistics are sent instead
  if (settings.ads) {
    settings.ads = false
    settings.usageStats = true
  } else {
    settings.ads = true
    settings.usageStats = false
  }
  return settings
}

function anagramsSwitched (settings) {
  if (settings.anagramsVisible) {
    settings.anagramsVisible = false
  } else {
    settings.anagramsVisible = true
  }
  return settings
}

function offlineChacheSwitched (settings) {
  if (settings.offlineCache) {
    settings.offlineCache = false
  } else {
    settings.offlineCache = true
  }
  return settings
}

function wwfSwitched (settings) {
  if (settings.wwfVisible) {
    settings.wwfVisible = false
  } else {
    settings.wwfVisible = true
  }
  return settings
}

export { anagramsSwitched, historySwitched, sourceSwitched, rhymesSwitched, antonymsSwitched, synonymsSwitched, similarSwitched, pagingSwitched, examplesSwitched, hideEmptyPages, adsSwitched, offlineChacheSwitched, wwfSwitched }

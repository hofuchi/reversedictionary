
import { AsyncStorage, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'
import reverseDictionaryRequest from './../apiRequests/ReverseDictionaryRequest'
import fetchUnknownWord from './../apiRequests/FetchUnknownWord'
import addToHistory from './../utilities/History'
import checkNetwork from './../utilities/Checks'

async function onSubmit (word, height, width, settings) {
  // When user presses the submit button to search for a word
  var similar

  addToHistory(
    word,
    height,
    settings
  )

  if (word.match(/\*|\b\w+\b|\*/g).length > 1) {
    // ?Transition to while loop for multiple asterisks?
    // Add support for question marks that aren't the ending char
    let cached = await AsyncStorage.getItem(word)

    if (settings.offlineCache && cached !== null) {
      let cachedData = JSON.parse(await AsyncStorage.getItem(word))
      similar = cachedData
    } else {
      if (word.indexOf('*') >= 0 && word.match(/\b(\*|\w)+\b|\*/g).length <= 2) {
        similar = await fetchUnknownWord(word)
      } else {
        similar = await reverseDictionaryRequest(word)
      }

      if (settings.offlineCache && checkNetwork) {
        await AsyncStorage.setItem(word, JSON.stringify(similar))
      }
    }

    Actions.search({
      hide: true,
      similar: similar,
      autoFocus: true,
      searchedWord: word
    })
  } else {
    Actions.searchResult({
      word: word,
      title: word,
      height: height,
      width: width,
      settings: settings
    })
  }
};

export default onSubmit

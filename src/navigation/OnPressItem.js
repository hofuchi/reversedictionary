
import { AsyncStorage } from 'react-native'
import { Actions } from 'react-native-router-flux'
import reverseDictionaryRequest from './../apiRequests/ReverseDictionaryRequest'
import fetchUnknownWord from './../apiRequests/FetchUnknownWord'
import addToHistory from './../utilities/History'

function onPressItemSearchResultPage (word, settings, height, width) {
  // Hackish way to push the same scene without breaking anything
  word = (JSON.stringify(word)).slice(1, -1)

  if (Actions.currentScene === 'searchResult') {
    Actions.searchResult2({
      word: word,
      title: word,
      width: width,
      height: height,
      settings: settings
    })
  } else if (Actions.currentScene === 'searchResult2') {
    Actions.searchResult3({
      word: word,
      title: word,
      width: width,
      height: height,
      settings: settings
    })
  } else if (Actions.currentScene === 'searchResult3') {
    Actions.searchResult4({
      word: word,
      title: word,
      width: width,
      height: height,
      settings: settings
    })
  } else if (Actions.currentScene === 'searchResult4') {
    Actions.searchResult5({
      word: word,
      title: word,
      width: width,
      height: height,
      settings: settings
    })
  }
}

async function onPressItemSearchPage (word, height, width, settings, hist, similar) {
  // User presses an item in their history list or similar list
  word = (JSON.stringify(word)).slice(1, -1)

  addToHistory(
    word,
    height,
    settings
  )

  if (word.match(/\*|\b\w+\b|\*/g).length > 1) {
    // ?Transition to while loop for multiple asterisks?
    // Add support for question marks that aren't the ending char
    let cached = await AsyncStorage.getItem(word)

    if (settings.offlineCache && cached !== null) {
      let cachedData = JSON.parse(await AsyncStorage.getItem(word))
      similar = cachedData
    } else {
      if (word.indexOf('*') >= 0 && word.match(/\b(\*|\w)+\b|\*/g).length <= 2) {
        similar = await fetchUnknownWord(word)
      } else {
        similar = await reverseDictionaryRequest(word)
      }

      if (settings.offlineCache && checkNetwork) {
        await AsyncStorage.setItem(word, JSON.stringify(similar))
      }
    }

    Actions.search({
      hide: true,
      similar: similar,
      autoFocus: true,
      searchedWord: word
    })
  } else {
    Actions.searchResult({
      word: word,
      title: word,
      height: height,
      width: width,
      settings: settings
    })
  }
};

export { onPressItemSearchResultPage, onPressItemSearchPage }

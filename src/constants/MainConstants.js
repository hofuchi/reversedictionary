import React from 'react'
import {
  View,
  TouchableOpacity } from 'react-native'
import styles from './../styles/MainStyles'
import {
  Reducer,
  Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/MaterialIcons'
// Might switch to realm for storing settings for speed over AsyncStorage
// const Realm = require('realm');

function refreshOnBack () {
  // componentWillReceiveProps is only called for a new props
  // ... hence Math.random()
  // Either the above or there was a cacheing issue switching to the
  // ... release version on my phone
  setTimeout(() => { Actions.refresh({'back': Math.random()}) }, 300)
}

function aboutButton () {
  return (
    <View>
      <TouchableOpacity style={styles.touchableRight} onPress={Actions.about}>
        <Icon style={styles.iconRight} name='info' size={28} />
      </TouchableOpacity>
    </View>
  )
}

function homeButton () {
  return (
    <View>
      <TouchableOpacity style={styles.touchableRight} onPress={() => Actions.popTo('search')}>
        <Icon style={styles.iconHome} name='home' size={32} />
      </TouchableOpacity>
    </View>
  )
}

function settingsButton () {
  return (
    <View>
      <TouchableOpacity style={styles.touchableLeft} onPress={Actions.settings}>
        <Icon style={styles.iconLeft} name='settings' size={28} />
      </TouchableOpacity>
    </View>
  )
}

const reducerCreate = params => {
  const defaultReducer = new Reducer(params)
  return (state, action) => {
    return defaultReducer(state, action)
  }
}

export { refreshOnBack, homeButton, settingsButton, reducerCreate, aboutButton }

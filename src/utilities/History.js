import { AsyncStorage } from 'react-native'
/* Dev - settings */
import { loadHistory } from './../settings/Settings'

async function addToHistory (word, height, settings) {
  let hist = await loadHistory()

  if (!settings.historyDisabled) {
    // Should take up ~(6 * screen size) but no more
    let maxHist = Number(settings.historyLength)
    // hist is null for first time users
    hist = hist || []
    // Should take up screen size but no more
    if (hist.length >= maxHist) {
      hist.pop()
      if (await AsyncStorage.getItem(hist[-1]) !== null) {
        await AsyncStorage.removeItem(hist[-1])
      }
      // In case I change this and user's list is longer than what
      // ... it should be or an ad is taking up part of the screen
      if (hist.length >= maxHist || settings.ads) {
        hist.pop()
        if (await AsyncStorage.getItem(hist[-1]) !== null) {
          await AsyncStorage.removeItem(hist[-1])
        }
      }
    }
    // Add word if it's not already there, remove and replace at top of list if it is
    let index = hist.indexOf(word)
    if (index > -1) {
      hist.splice(index, 1)
    }
    hist.unshift(word)

    try {
      await AsyncStorage.setItem('history', JSON.stringify(hist))
    } catch (err) {
      // console.log(err)
    }
  }
};

export default addToHistory


import {
  ToastAndroid,
  Platform,
  NetInfo } from 'react-native'

function checkNetwork () {
  // Checks if the user is connected to a network
  /* let networkOffline = setInterval(function () {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (!isConnected) {
        if (Platform.OS === 'android') {
          ToastAndroid.show(('Network is offline'), ToastAndroid.SHORT)
        }
      } else {
        clearInterval(networkOffline)
      }
    })
  }, 2000) */

  NetInfo.isConnected.fetch().then(isConnected => {
    if (!isConnected) {
      if (Platform.OS === 'android') {
        ToastAndroid.show(('Network is offline'), ToastAndroid.SHORT)
      }
      return false
    }
    return true
  })
}

export default checkNetwork


import {
  ToastAndroid,
  Platform,
  Clipboard } from 'react-native'

function copyToClipboard (word) {
  // Copies the selected item to the clipboard
  Clipboard.setString(word)
  if (Platform.OS === 'android') {
    ToastAndroid.showWithGravity('' + word + ' copied to clipboard',
                                 ToastAndroid.SHORT, ToastAndroid.BOTTOM)
  }
}

export default copyToClipboard

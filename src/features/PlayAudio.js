import RNAudioStreamer from 'react-native-audio-streamer'

function playAudio () {
  // Plays pronumciation audio when clicked
  RNAudioStreamer.status((err, status) => {
    if (err) {}
    if (status !== 'PLAYING' && status !== 'FINISHED') {
      RNAudioStreamer.play()
    } else if (status === 'FINISHED') {
      RNAudioStreamer.seekToTime(0) // So user can listen multiple times
      RNAudioStreamer.play()
    }
  })
};

export default playAudio


import React from 'react'
import {
  View,
  Text } from 'react-native'
// Libraries
import RNAudioStreamer from 'react-native-audio-streamer'
import Icon from 'react-native-vector-icons/MaterialIcons'
// Dev - misc
import styles from './../styles/StyleWrapper'
import playAudio from './../features/PlayAudio'

function audioFooter (audioUri) {
  // Button to display if an audio pronumciation is available
  if (audioUri) {
    RNAudioStreamer.setUrl(audioUri)
    return (
      <View><Icon.Button onPress={playAudio} style={styles.audioButton} name='volume-up'><Text style={styles.audioText}>Pronunciation</Text></Icon.Button></View>
    )
  } else {
    return (
      <Text style={[styles.titleText, styles.textAlignCenter]}>---</Text>
    )
  }
}

export default audioFooter

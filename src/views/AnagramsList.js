
import React from 'react'
import {
  View,
  FlatList } from 'react-native'
import { ListItem } from 'react-native-elements'
import copyToClipboard from './../utilities/CopyToClipboard'
import { onPressItemSearchResultPage } from './../navigation/OnPressItem'
import listEmptyView from './ListEmptyView'
import footer from './Footers'
import { anagramHeader } from './Headers'

function anagramsList (anagrams, overrides, settings, height, width, word) {
  let listOverride = overrides.listOverride
  let widthOverride = overrides.widthOverride

  return (
    <View style={widthOverride}>
      <FlatList
        style={listOverride}
        data={anagrams}
        renderItem={({ item }) => (
          <ListItem
            onPress={() => onPressItemSearchResultPage(
              item,
              settings,
              height,
              width
            )}
            onPressRightIcon={() => onPressItemSearchResultPage(
              item,
              settings,
              height,
              width
            )}
            titleNumberOfLines={1}
            onLongPress={() => copyToClipboard(item)}
            title={item} />
        )}
        initialNumToRender={14}
        enableEmptySections
        ListFooterComponent={() => footer()}
        ListHeaderComponent={anagramHeader(word)}
        ListEmptyComponent={listEmptyView}
        keyExtractor={(item, index) => index} />
    </View>
  )
}

export default anagramsList


import React from 'react'
import { View, Text } from 'react-native'

function listEmptyView () {
  // Renders the default footer
  return (
    <View><Text>No Data</Text></View>
  )
};

export default listEmptyView

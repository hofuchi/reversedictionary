
import React from 'react'
import { View, Text } from 'react-native'
import styles from './../styles/StyleWrapper'
import { StyleSheet } from 'react-native'

const headerStyle = [styles.titleText, styles.textBold, styles.paddingVertical6, styles.paddingHorizontal12]

function rhymeHeader (word) {
  return (
    <View><Text style={headerStyle}>Rhymes:</Text></View>
  )
};

function similarHeader (word) {
  return (
    <View><Text style={headerStyle}>Similar Words:</Text></View>
  )
};

function synonymHeader (word) {
  return (
    <View><Text style={headerStyle}>Synonyms:</Text></View>
  )
};

function antonymHeader (word) {
  return (
    <View><Text style={headerStyle}>Antonyms:</Text></View>
  )
};

function exampleHeader (word) {
  return (
    <View><Text style={headerStyle}>Example Sentences:</Text></View>
  )
};

function historyHeader () {
  return (
    <View><Text style={headerStyle}>History:</Text></View>
  )
};

function plainHeader (word) {
  return (
    <View><Text style={headerStyle}>{word}</Text></View>
  )
};

function anagramHeader (word) {
  return (
    <View><Text style={headerStyle}>Anagrams:</Text></View>
  )
};

function definitionHeader (word, score, settings) {
  return (
    <View><Text
      style={headerStyle}>Definitions of {word}:
      <Text
        style={styles.scrabbleText}>{'\n\t'}{settings.wwfVisible ? "Words with Friends" : "Scrabble®"} score: {score}
      </Text>
    </Text></View>
  )
};

export default plainHeader
export {
  rhymeHeader,
  similarHeader,
  synonymHeader,
  antonymHeader,
  exampleHeader,
  historyHeader,
  definitionHeader,
  anagramHeader
}

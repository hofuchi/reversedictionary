
import React from 'react'
import {
  View,
  FlatList } from 'react-native'
import { ListItem } from 'react-native-elements'
import copyToClipboard from './../utilities/CopyToClipboard'
import listEmptyView from './ListEmptyView'
import audioFooter from './AudioFooter'
import { scrabbleScore, wwfScore } from './../features/ScrabbleScore'
import { definitionHeader } from './Headers'
import styles from './../styles/StyleWrapper'

function definitionsList (definitions, overrides, word, audioUri, settings) {
  // Code to display the list of example sentences
  if (settings.wwfScore) {
    var score = wwfScore(word.toLowerCase())
  } else {
    score = scrabbleScore(word.toLowerCase())
  }
  let listOverride = overrides.listOverride
  let widthOverride = overrides.widthOverride

  return (
    <View style={widthOverride}>
      <FlatList
        style={listOverride}
        data={definitions}
        renderItem={({ item }) => (
          <ListItem
            hideChevron
            titleNumberOfLines={8}
            title={item.text || item.substring(item.indexOf('\t') + 1)}
            subtitle={item.text ? null : item.substring(0, item.indexOf('\t'))}
            onLongPress={() => copyToClipboard(item.text || item.substring(item.indexOf('\t') + 1))}
          />
        )}
        ListFooterComponent={() => audioFooter(audioUri)}
        ListHeaderComponent={definitionHeader(word, score, settings)}
        initialNumToRender={10}
        ListEmptyComponent={listEmptyView}
        keyExtractor={(item, index) => index} />
    </View>
  )
}

export default definitionsList


import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import styles from './../styles/StyleWrapper'
import searchPageStyles from './../styles/SearchPageStyles'
import mainStyles from './../styles/MainStyles'
import { secretAdModBannerKey } from './../secrets/secrets'
// Below import currently not working with jest for SearchPageTest.js
// ...TypeError: Cannot read property 'admob' of undefined
import { Banner, adRequest } from './../constants/Ads'

function defaultFooter () {
  // Renders the default footer
  return (
    <View><Text style={[styles.titleText, styles.textAlignCenter]}>---</Text></View>
  )
};

function footer (settings = null) {
  if (settings && settings.ads) {
    return (
      <View style={mainStyles.bannerAd}>
        <Banner
          size={'BANNER'}
          unitId={secretAdModBannerKey}
          request={adRequest.build()}
        />
      </View>
    )
  } else {
    return defaultFooter()
  }
};

export default footer

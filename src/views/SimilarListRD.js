
import React from 'react'
import {
  View,
  Text,
  FlatList } from 'react-native'
import { ListItem } from 'react-native-elements'
import copyToClipboard from './../utilities/CopyToClipboard'
import { onPressItemSearchPage } from './../navigation/OnPressItem'
import plainHeader from './Headers'
import footer from './Footers'

function similarListRD (similar, listOverride, searchedWord, height, width, settings, hist) {
  // Returns previous search history

  return (
    <FlatList
      style={listOverride}
      data={similar}
      renderItem={({ item }) => (
        <ListItem
          onPressRightIcon={() => onPressItemSearchPage(
            item.word,
            height,
            width,
            settings,
            hist,
            similar
          )}
          onPress={() => onPressItemSearchPage(
            item.word,
            height,
            width,
            settings,
            hist,
            similar
          )}
          titleNumberOfLines={1}
          onLongPress={() => copyToClipboard(item.word)}
          title={item.word}
        />
      )}
      ListEmptyComponent={<View><Text>No Data</Text></View>}
      keyExtractor={(item, index) => index}
      initialNumToRender={8}
      ListFooterComponent={() => footer(settings)}
      ListHeaderComponent={plainHeader(searchedWord)}
    />
  )
}

export default similarListRD

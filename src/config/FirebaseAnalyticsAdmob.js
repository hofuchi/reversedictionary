import firebase from './Firebase'
import { secretAdModAppKey } from './../secrets/secrets'

function initiateAnalyticsCollection () {
  firebase.analytics().setAnalyticsCollectionEnabled(true)
  firebase.crash().setCrashCollectionEnabled(true)
  // firebase.perf().setPerformanceCollectionEnabled(true)
}

function stopAnalyticsCollection () {
  firebase.analytics().setAnalyticsCollectionEnabled(false)
  firebase.crash().setCrashCollectionEnabled(false)
  // firebase.perf().setPerformanceCollectionEnabled(false)
}

function initiateAnalyticsORAds (adsEnabled) {
  if (adsEnabled) {
    initializeAdmob()
  } else {
    initiateAnalyticsCollection()
  }
}

function initializeAdmob () {
  firebase.admob().initialize(secretAdModAppKey) // TEST_KEY // ca-app-pub-3940256099942544~3347511713
}

export { initiateAnalyticsORAds, stopAnalyticsCollection }

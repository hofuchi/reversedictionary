
import { StyleSheet } from 'react-native'

const borderStyles = StyleSheet.create({
  borderBottom: {
    borderBottomWidth: 2,
    color: '#000' // Black
  }
})

export default borderStyles

import { StyleSheet } from 'react-native'
// import textStyles from './TextStyles'
import aboutPageStyles from './AboutPageStyles'
import searchPageStyles from './SearchPageStyles'
import searchResultPageStyles from './SearchResultPageStyles'
import settingsPageStyles from './SettingsPageStyles'
import mainStyles from './MainStyles'
import borderStyles from './BorderStyles'
import textStyles from './TextStyles'
import spacingStyles from './SpacingStyles'
import alignStyles from './AlignStyles'

const styleWrapper = StyleSheet.flatten([
  aboutPageStyles,
  searchResultPageStyles,
  settingsPageStyles,
  searchPageStyles,
  borderStyles,
  textStyles,
  spacingStyles,
  alignStyles,
  mainStyles
])

export default styleWrapper

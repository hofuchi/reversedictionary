
import { StyleSheet } from 'react-native'

const searchResultPageStyles = StyleSheet.create({
  // StyleSheets
  container: {
    paddingHorizontal: 16
  },
  audioText: {
    color: '#FFFFFF', // White
    fontSize: 16
  },
  scrabbleText: {
    color: '#A1A1A1', // Black
    fontSize: 13,
    paddingBottom: 10
  },
  list: {
    elevation: 15,
    marginVertical: 0,
    paddingBottom: 0,
    backgroundColor: '#FFFFFF' // Light Grey
  },
  audioButton: {
    justifyContent: 'center',
    elevation: 5,
    height: 48,
    backgroundColor: '#0091EA' // Light Blue
  }
})

export default searchResultPageStyles


import { StyleSheet } from 'react-native'
import searchResultStyles from './SearchResultPageStyles'
import searchPageStyles from './SearchPageStyles'

function searchResultOverrides (lessenWidth = false, height, width, pagingEnabled) {
  let listOverride = StyleSheet.flatten([
    searchResultStyles.list,
    {minHeight: height, maxHeight: height}
  ])
  if (!pagingEnabled && !lessenWidth) {
    var widthOverride = StyleSheet.flatten([
      searchResultStyles.container,
      {minWidth: width / 1.5, maxWidth: width / 1.5}
    ])
  } else {
    widthOverride = StyleSheet.flatten([
      searchResultStyles.container,
      {minWidth: width, maxWidth: width}
    ])
  }
  return {
    listOverride: listOverride,
    widthOverride: widthOverride
  }
}

function searchOverrides (height, viewHeight) {
  // 56 is height of navbar
  // listOverride = StyleSheet.flatten([
  //  searchStyles.list,
  //  {alignSelf: 'stretch', flex: 1}
  // ])
  return searchPageStyles.list
}

export { searchOverrides, searchResultOverrides }

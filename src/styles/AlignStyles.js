
import { StyleSheet } from 'react-native'

const alignStyles = StyleSheet.create({
  alignCenter: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  textAlignCenter: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default alignStyles

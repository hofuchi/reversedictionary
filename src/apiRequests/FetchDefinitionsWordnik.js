
import { ToastAndroid, Platforms } from 'react-native'
// import fetch from 'unfetch'
import secretAPIKey from './../secrets/secrets'
import handleErrors from './../utilities/HandleFetchErrors'
import fetchDefinitionsDatamuse from './FetchDefinitionsDatamuse'

async function fetchDefinitionsWordnik (word, loop) {
  var definitions
  let uri = 'http://api.wordnik.com:80/v4/word.json/' + word + '/definitions?limit=200&includeRelated=false&useCanonical=true&includeTags=false&api_key=' + secretAPIKey + ''
  await fetch(uri).then(handleErrors).then(res => res.json()).then(async res => {
    if (res.length < 1 && loop === 1) {
      definitions = await fetchDefinitionsDatamuse(word, 2)
    } else {
      definitions = res
    }
  }).catch(error => {
    console.log(error)
    if (Platform.OS === 'android') {
      ToastAndroid.show(('Unable to connect'), ToastAndroid.SHORT)
    }
    definitions = ['No Data']
  })
  if (JSON.stringify(definitions) === '[]') {
    definitions = ['No Data']
  }

  return definitions
}

export default fetchDefinitionsWordnik


// import { ToastAndroid, Platform } from 'react-native'
// import fetch from 'unfetch'
import handleErrors from './../utilities/HandleFetchErrors'
import secretAPIKey from './../secrets/secrets'

function fetchWordnik (word, hideEmptyPages, max) {
  // Fetches the WOTD
  let uri = 'http://api.wordnik.com:80/v4/word.json/' + word + '/relatedWords?useCanonical=true&limitPerRelationshipType=' + max + '&api_key=' + secretAPIKey + ''
  return new Promise((resolve, reject) => {
    fetch(uri).then(handleErrors).then(res => res.json()).then(res => {
      for (let i = 0; i < res.length; i++) {
        if (JSON.stringify(res[i].relationshipType) === '"synonym"') {
          var syn = res[i].words
        } else if (JSON.stringify(res[i].relationshipType) === '"antonym"') {
          var ant = res[i].words
        } else if (JSON.stringify(res[i].relationshipType) === '"rhyme"') {
          var rhy = res[i].words
        } else if (JSON.stringify(res[i].relationshipType) === '"hypernym"') {
          var hyp = res[i].words
        }
      }

      if (!syn && hideEmptyPages) {
        syn = []
      }
      if (!ant && hideEmptyPages) {
        ant = []
      }
      if (!rhy && hideEmptyPages) {
        rhy = []
      }
      if (!hyp && hideEmptyPages) {
        hyp = []
      }

      let returnData = {
        synonyms: syn,
        antonyms: ant,
        rhymes: rhy,
        similar: hyp
      }
      resolve(returnData)
    }).catch(error => {
      console.log(error)
      // if (Platform.OS === 'android') {
      //  ToastAndroid.show(('Part of the API may be down'), ToastAndroid.SHORT)
      // }
      let returnData = {
        synonyms: [],
        antonyms: [],
        rhymes: [],
        similar: []
      }
      reject(returnData)
    })
  })
}

export default fetchWordnik


import { ToastAndroid, Platform } from 'react-native'
// import fetch from 'unfetch'
import handleErrors from './../utilities/HandleFetchErrors'

function fetchUnknownWord (word) {
  // Fetches a word with a wildcard in it
  let uri = 'https://api.datamuse.com/words?sp=' + word + ''
  return new Promise((resolve, reject) => {
    fetch(uri).then(handleErrors).then(res => res.json()).then(res => {
      let predictedWords = res
      resolve(predictedWords)
    }).catch(error => {
      console.log(error)
      if (Platform.OS === 'android') {
        ToastAndroid.show(('Unable to connect'), ToastAndroid.SHORT)
      }
      reject(error)
    })
  })
}

export default fetchUnknownWord


import { ToastAndroid, Platform } from 'react-native'
// import fetch from 'unfetch'
import handleErrors from './../utilities/HandleFetchErrors'
import fetchDefinitionsWordnik from './FetchDefinitionsWordnik'

async function fetchDefinitionsDatamuse (word, loop) {
  var definitions
  let uri = 'https://api.datamuse.com/words?sp=' + word + '&md=d&max=1'
  await fetch(uri).then(handleErrors).then(res => res.json()).then(async res => {
    if ((res.length < 1 || !res || !res[0].defs) && loop === 1) {
      definitions = await fetchDefinitionsWordnik(word, 2)
    } else {
      definitions = res[0].defs
    }
  }).catch(error => {
    console.log(error)
    if (Platform.OS === 'android') {
      ToastAndroid.show(('Unable to connect'), ToastAndroid.SHORT)
    }
    definitions = ['No Data']
  })
  if (JSON.stringify(definitions) === '[]') {
    definitions = ['No Data']
  }

  return definitions
}

export default fetchDefinitionsDatamuse

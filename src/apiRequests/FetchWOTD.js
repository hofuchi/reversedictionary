
// import { ToastAndroid, Platform } from 'react-native'
// import fetch from 'unfetch'
import secretAPIKey from './../secrets/secrets'
import handleErrors from './../utilities/HandleFetchErrors'

function fetchWOTD () {
  // Fetches the WOTD
  let uri = 'http://api.wordnik.com:80/v4/words.json/wordOfTheDay?api_key=' + secretAPIKey + ''
  return new Promise((resolve, reject) => {
    fetch(uri).then(handleErrors).then(res => res.json()).then(res => {
      if (res) {
        var wotd = res.word
      } else {
        wotd = undefined
      }
      resolve(wotd)
    }).catch(error => {
      // console.log(error)
      if (Platform.OS === 'android') {
        ToastAndroid.show(('You are offline'), ToastAndroid.SHORT)
      }
      reject(undefined)
    })
  })
}

export default fetchWOTD

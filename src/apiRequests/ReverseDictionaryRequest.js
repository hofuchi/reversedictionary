
import { ToastAndroid, Platform } from 'react-native'
// import fetch from 'unfetch'
import handleErrors from './../utilities/HandleFetchErrors'

function reverseDictionaryRequest (word) {
  // Fetches the WOTD
  let uri = 'https://api.datamuse.com/words?ml=' + word + '&max=32'
  return new Promise((resolve, reject) => {
    fetch(uri).then(handleErrors).then(res => res.json()).then(res => {
      let similar = res.filter(function (item) {
        return item.word.length > 2
      })
      resolve(similar)
    }).catch(error => {
      console.log(error)
      if (Platform.OS === 'android') {
        ToastAndroid.show(('Unable to connect'), ToastAndroid.SHORT)
      }
      reject(error)
    })
  })
}

export default reverseDictionaryRequest

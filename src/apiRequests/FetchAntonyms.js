
// import { ToastAndroid, Platform } from 'react-native'
// import fetch from 'unfetch'
import handleErrors from './../utilities/HandleFetchErrors'

function fetchAntonyms (word, hideEmptyPages, max) {
  // Fetches the WOTD
  let uri = 'https://api.datamuse.com/words?rel_ant=' + word + '&max=' + max + ''
  return new Promise((resolve, reject) => {
    fetch(uri).then(handleErrors).then(res => res.json()).then(res => {
      if (res.length < 1 && hideEmptyPages) {
        var antonyms = []
      } else {
        antonyms = res
      }
      resolve(antonyms)
    }).catch(error => {
      console.log(error)
      // if (Platform.OS === 'android') {
      //  ToastAndroid.show(('Part of the API may be down'), ToastAndroid.SHORT)
      // }
      reject([])
    })
  })
}

export default fetchAntonyms
